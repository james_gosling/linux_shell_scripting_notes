

# 处理信号
## 常见信号
| 信号 | 值 | 描述 |
|:---|:---:|---:|
|1|SIGHUP|挂起|
|2|SIGINT|终止|
|3|SIGQUIT|停止|
|9|SIGKILL|无条件终止|
|15|SIGTERM|尽可能终止|
|17|SIGSTOP|无条件停止，但不是终止|
|18|SIGTSTP|停止或暂停，但不终止|
|19|SIGCONT|继续运行停止的进程|

默认情况下，bash shell会忽略收到的 `3` 和 `5` 信号，但是会处理收到的 `1` 和 `2` 信号  
对于 `1` 和 `2` ，bash shell收到后会传递给由它启动的进程

## 生成信号
### 中断
使用 `Ctrl` + `C` 快捷键可以发送 *SIGINT* 信号  

### 暂停
使用 `Ctrl` + `Z` 快捷键可以发送 *SIGTSTP* 信号  

## 捕获信号
在脚本中使用 `trap COMMANDs SIGNALs` 来做信号捕获，被捕获后改信号不会再交给shell处理  
比如 `trap "echo 'I have trapped Ctrl-C.'" SIGINT` 会让脚本不会对 *Ctrl C* 做出实际回应  

### 捕获退出信号
`trap COMMANDs EXIT` 即可  
可以用来在脚本执行完成后做些动作  

### 修改或移除捕获
修改只需在捕获后的代码段中再次使用 `trap COMMANDs THE_SAME_SIGNAL` 即可  
移除则是使用 `trap -- THE_SAME_SIGNAL`  
<sup>Tips:</sup>也可以使用 `trap -` 来恢复信号的默认行为  

# 后台模式运行脚本
在命令末尾使用 `&` 即可让脚本在后台运行  
即使脚本在后台运行，它还是会在终端显示器来打印 *STDOUT* 和 *STDERR* 信息，所以，必要时在脚本中做好它们的重定向  

## 在非控制台下运行脚本 `nohup`
使用 `&` 运行在后台的脚本，都会跟当前的终端会话关联在一起，如果退出当前的终端会话，这些后台进程也会随之退出  
使用 `nohup` 可以实现退出终端会话后，后台进程继续执行的效果  
它为运行脚本的进程阻断了 *SIGHUP* 信号，也解除了该进程与终端会话的关联  
所以 *STDOUT* 和 *STDERR* 也不会传递到当前的终端显示器上，而是会重定向到脚本所在目录的 `nohup.out` 文件中  

# 作业控制
## 查看作业 `jobs`
可以显示分配给shell的作业  
`[N]` 方括号中是作业的ID  
带 `+` 的是默认作业，带 `-` 的是下一条默认作业  

## 重启停止的作业 `bg`/`fg`
`bg` 和 `fg` 分别用于将作业放到 *后台*/*前台* 执行  

# 调整谦让度
谦让度取值范围从 `-20`(最高优先级) ~ `19`  
## `nice`
用于在启动命令时就指定其谦让度 `nice -n NICE_VAL COMMAND` 即可  
`-n NICE_VAL` 可以简写成 `-NICE_VAL`  

## `renice`
重新调整进程的优先级  
> <h2>📄Tips:</h2>
> 
> 对于 *非root* 用户，`renice` 有这些限制
>
> * 只能对属于自己的进程 renice
>
> * 只能调低进程的谦让度
>
> root用户则不受这些限制

# 定时运行作业
## `at`
定时执行脚本，一般命令是 `at -f SCRIPT_PATH TIME_SPEC`  
比如： `at -f /root/a_script.sh now`  
使用 *at* 命令，作业会被提交到 **作业队列**，作业队列通常用 `a~z`、`A~Z` 来指代  
作业队列字母排序越高，作业的运行优先级就越低。默认情况下 *at* 会把作业提交到 **a** 队列  
> 早年间，也有用 `batch` 定时执行脚本的用法
>
> 不过现在 `batch` 已经演化为一个shell脚本 `/usr/bin/batch`，它会调用 `at` 并把作业提交到 **b** 队列中
> 

### 作业的输出
作业默认将提交作业用户的电子邮件地址作为 *STDOUT* 和 *STDERR*  
同时 `at` 使用 `sendmail` 来发送邮件，所以如果系统中未安装，作业则无任何输出  
所以，如果不需要，记得在脚本中做好重定向，对 `at` 使用 `-M` 参数来屏蔽发送邮件功能  

### 列出作业、删除作业
列出作业使用 `atq`  
删除作业使用 `atrm`  

## 定期执行脚本 `cron`
### cron 时间表
格式是： *min hour dayofmonth month dayofweek command*  
> <h2>📄Tips:</h2>
>
> 如何让脚本每月最后一天执行？
>
> 通过检查明天是不是1号来实现
>
> 00 12 * * * if [ `date +%d -d tomorrow` = 01 ] ; then ; command
> 

### 操作 cron 时间表
`crontab -l` 查看当前用户的 cron 时间表  
`crontab -e` 用来编辑 cron 计划  

### cron 目录
可以使用 `/etc/cron.hourly`、`daily`、`weekly`、`monthly` 来存放脚本，*cron* 会按照文件夹对应的周期来执行它们  

### anacron 程序
`anacron` 可以获取系统关机期间未执行的 *cron* 任务，并尽快执行这些作业  
它只会处理 *cron目录* 的程序，并且 **不会处理执行时间需求小于1天的脚本**，即 */etc/cron.hourly* 中的脚本  
它的配置文件在 `/etc/anacrontab`  
定义格式为 `PERIOD  DELAY   IDENTIFIER  COMMAND`  

## 使用新shell启动脚本
> 这部分没有看太懂
>
> 感觉还是在讲 登录shell 跟 其它shell 的区别
>
> 登录时运行的脚本放在 `$HOME/.bash_profile`/`$HOME/.bash_login`/`$HOME/.profile` 三个文件的任意一个中（bash shell会从左往右依次寻找它们）
>
> 每次启动shell都要运行的脚本放在 `$HOME/.bashrc` 中
>

