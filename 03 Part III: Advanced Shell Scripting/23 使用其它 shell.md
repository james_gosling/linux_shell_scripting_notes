

# dash
前文已经做过介绍，*dash* 是 **Debian** 下的 *ash*  
*ash shell* 则是 *Unix* 上的 *Bourne shell* 的简化版本，全名是 ***Almquist SHell***，它的最早版本的特点为：  
**体积小**、**速度快**，但是 **缺少很多高级功能**  

## 不能使用的功能
### 算数运算方面
可以使用 `expr OPERATION` 或者 `$(( OPERATION ))`，无法使用 `$[ OPERATION ]`  

### 文本比较方面
*bash* 使用 `==` 来判断两个文本是否一致，*dash* 中使用 `=` 来实现该功能，它无法识别 `==`  

### 定义函数方面
*dash* 使用如下格式定义函数：  
```
FUNC_NAME() {
    OPERATIONs
}
```
它不支持 *bash* 另一种定义方式 `function FUNC_NAME { ... }` 

# zsh
它是一个 *Unix shell*  
它的一些独特功能：`改进的shell选项处理`、`shell兼容性模式`、`可加载模块`  

## 相较于 *bash* 的增强点
### 数学运算
在 `let` 和 `$(())` 数学运算中支持浮点运算（*bash* 则不支持）  
<sup>补充：</sup> 可以加载 `zsh/mathfunc` 模块，扩展很多数学函数  

### 结构化命令强化 `repeat`
```
repeat PARAM
do
    OPERATIONs
done
```
其中的 *PARAM* 为 **数字** 或者 **可以计算出数值的算数式**  
这样，*do* 语句块会重复执行 *PARAM* 次  

