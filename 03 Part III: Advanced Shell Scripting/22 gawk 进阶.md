

# 使用变量
## 内建变量
### 字段和记录分隔符变量
`gawk` 默认使用 *空白字符* 作为分隔符  
*gawk* 中有`FIELDWIDTHS`、`FS`、`RS`、`OFS`、`ORS` 等字段分隔所使用的内建变量  
`FS` 用来指定 输入字段分隔符 ，`RS` 用来指定 输入记录分隔符  
带上 `O`(*OFS*、*ORS*) 则表示输出时的选项  
`FIELDWIDTHS` 则是指定各字段的 字符串长度 ，<sup>⚠️注意：</sup> **不适用于 *变长* 的字段**  
```
# cat data
1005.3247596.37
115-2.349194.00
05810.1298100.1
# gawk 'BEGIN{FIELDWIDTHS="3 5 2 5"}{print $1,$2,$3,$4}' data
100 5.324 75 96.37
115 -2.34 91 94.00
058 10.12 98 100.1
```

### 数据变量
常用的有 `NR`、`FNR`、`NF` 等  
`NF` 表示 当前处理行包含的字段数  
`FNR` 表示 当前文件的处理行数，`NR` 则表示 *gawk* 本次处理的行数。这两个值只有在 *gawk* 处理多个文件时可能会不一致，处理单个文件它们始终是相同的  


## 自定义变量
可以在 *gawk* 命令中给变量赋值，也可以在其中给已经赋值的变量重新赋值  
```
# gawk '
> BEGIN{
> testing="this is a test"
> print testing
> testing=45
> }'
```
也可以在 *gawk* 命令行中定义变量，比如 `gawk 'BEGIN{FS=","} {print $n}' n=2 data`   
这种方式定义的变量在 *BEGIN* 语句块中不会生效，使用 `-v` 参数可以让它在 *BEGIN* 语句块中同样生效<sup>说明：</sup> `-v` 需要放到 *gawk* 命令语句块之前，`gawk -v n=2 'BEGIN ...'`  

# 处理数组
## 定义数组变量
使用 `VAR[INDEX] = VALUE` 格式来进行数组中变量的定义  
```
capital["US"] = LOS

##or

var[1] = 89
```

## 遍历数组
```
for (TMP in ARRAY)
{
    OPERATIONs
}
```

## 删除数组变量
`delete ARRAY[INDEX]`  

# 关键字匹配
## 正则表达式
跟 `sed` 类似，*gawk* 支持 *BRE* 和 *ERE* 格式的正则表达式  

## 匹配操作符
可以指定哪个字段匹配正则表达式，使用类似 `$1 ~ /^PATTERN/` 格式来实现  
也可以使用排除符号来取反，`$1 !~ /^PATTERN/`  

## 数学表达式
使用 `==`、`>=`、`>`、`<` 等来做匹配  
数学表达式也可以作用于文本，但是必须完全匹配  

# 结构化命令
## *if* 语句
```
if (CONDITION)
    OPERATIONs

# or
if (CONDITION) OPERATIONs
```
如果 *if* 判断成功后需要执行多个语句，使用 `{ }` 包裹  

### *else* 扩展
```
if (CONDITION)
    OPERATIONs
else
    OPERATIONs

# or
if (CONDITION) OPERATIONs; else OPERATIONs
```
<sup>⚠️注意：</sup> *else* 写在一行时，*if* 语句后面需要有 `;`  

## *while* 语句
```
while (CONDITION)
{
    OPERATIONs
}
```
*while* 语句块中可以使用 *break* 和 *continue*  
### *do-while*
```
do
{
    OPERATIONs
} while (CONDITION)
```

## *for* 语句
`for( *variable assignment; condition; iteration process*)`  

# 格式化打印
和 **C语言** 的格式化输出类似，使用 `printf "FORMAT_STRING", VAR1, VAR2 ...` 的语法  

# 内建函数
## 数学函数
*gawk* 有一些内建的数学函数，比如 `rand()`、`int(x)`、`sqrt(x)`、`lshift(x, n)` 等  

## 字符串函数
`asort`、`asorti`、`split` 等函数
> <h2>📄备注：</h2>
>
> `asort` 和 `asorti` 分别是对 **数组值** 和 **数组索引** 进行排序并替换当前数组值
>
> 但是 *gawk* 输出数组默认并不会按照顺序来
>
> 比如四个元素的数组，使用 `for TEMP_VAR in ARRAY` 的方式进行输出，最终的输出顺序却是
>
> > 4 1 2 3
>
> 如果想要顺序输出，可以使用：
>
> `for (i = 1; i <= length(ARRAY); i++)` 的方式来顺序化取索引
>
> 或者使用 *gawk* 的 `PROCINFO["sorted_in"]` 属性，指定为类似 `"@ind_str_asc"` 的排序方式
>

## 时间函数
`systime`、`strftime`、`mktime` 等方法可以用来处理时间值  

# 自定义函数
```
function NAME([VARIABLEs])
{
    OPERATIONs
}
```
也可以使用 *函数库* 方式将多个函数写在文件中，随后在 `gawk` 中使用 `-f` 参数来进行引用  

