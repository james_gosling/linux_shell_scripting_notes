

# shell的类型
前文已经做过各种版本shell的大致介绍  
常见的有 `bash（Bourne-Again SHell）`、`tcsh(基于C shell的版本)`、`ash`、`dash(Debian版的ash)`、`zsh`  

# shell的父子关系
在当前的shell窗口中调用某个shell命令新生成的shell窗口，它们之间则为父子关系  
## 进程列表
```
# pwd; ls; cd /etc; echo $BASH_SUBSHELL

# (pwd; ls; cd /etc; echo $BASH_SUBSHELL)
```
将多个命令用括号包裹，会把他们组成一个命令组，并启动子shell来执行  
可以通过 `echo $BASH_SUBSHELL` 查看其输出来确定是否有子SHELL及个数  
> **说明：**
> 
> 除了 `(command group)` 这种方式之外，还可以使用 `{ command group; }` 的方式建立进程列表  
> 
> 这种方式不会创建子shell

## 别出心裁的子shell用法
### 后台模式 &
在命令末尾加上'&'表示将它放在后台运行，比如 `sleep 3000&`  
### 协程 `coproc`
书中的释义是：  
> 它在后台生成一个子shell，并在这个子shell中执行命令。
> 
没太明白它的意义，后来翻查了网络，CSDN[有篇文章](https://blog.csdn.net/yetugeng/article/details/88132458)讲，协作进程与父进程之间有双向管道  
方便两个shell之间的信息传递  

# 理解shell的内建命令
## 外部命令
不属于shell程序的命令，比如 `ps`  
外部命令执行时会创建出子进程，这种操作叫做 `衍生(forking)`  

## 内建命令
shell自带命令，比如 `cd`、`echo`  
可以使用 `type CMD` 来确认命令到底是内建的还是外部的  
### `history`
记录bash shell的历史命令  
shell退出时，历史命令会存到 `~\.bash_history` 中  
也可以使用 `history -a` 将当前的命令历史手动写入  

### `alias`
取命令别名  
别名只在它所被定义的shell进程中才有效  
> 下例中，子shell会提示父shell中定义的别名无效
> 
```
# alias li='ls -li'
# bash
# li
bash: li: command not found
```

