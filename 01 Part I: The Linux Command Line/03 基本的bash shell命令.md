

# 一些基本命令
## `man`
查看帮助

## `pwd`
显示当前所在目录  
> <sup>补充知识:</sup> **.** 和 **..**  
> 
> 表示当前目录(.) 和 所在目录的父级目录(..)
> 

## `ls`
`-F` 可以方便的分辨出文件和文件夹  
`-R` 可以对文件夹做递归显示  
`-l` 可以使用列表形式展示文件的更多信息  
> <sup>补充知识:</sup> 通配符的使用  
> 
> ls命令所接的文件参数, 可以使用通配符
> 
> ? 表示一个字符, * 表示任意多个字符
> 
> [aeiou] 表示多个选其一  
> 
> [a-s] 表示将 'a' 到 's' (包含首尾)的所有字符都放在选择列表中  
> 
> [!aeiou] 表示取反  
> 

## `touch`
创建空文件或者更新文件的修改时间/访问时间(使用 `-a` 参数)  

## `cp`
复制文件  

## `link`
建立文件链接  
`-s` 可以建立符号链接(软链接)  

## `mv`
移动文件  

## `rm`
删除文件  

## `mkdir`
创建目录  
如果一次需要递归创建多个，使用 `-p` 参数  

## `rmdir`
删除目录  
⚠️注意：它只能删除空目录，所以一般会使用 `rm -rf` 来做目录及其包含文件的删除  

## `file`
查看文件类型  

## `cat`/`more`/`less`
文本查看  

## `tail`/`head`
只查看文件的一部分  

