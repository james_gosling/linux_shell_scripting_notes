

# 什么是Linux?
Linux是一套操作系统, 它由`Linux内核`、`GNU工具`、`图形化桌面环境`、`应用软件`组成  
## Linux内核
内核由 `Linus Torvalds` 负责维护  
它主要负责 `系统内存管理`、`软件程序管理`、`硬件设备管理`、`文件系统管理`  
## GNU工具
包含 `GNU coreutils` 和 `shell`  
其中, *GNU coreutils* 用来 处理文件/操作文本/管理进程  
*shell* 用来与内核做交互  
## Linux桌面环境
一些概念普及 `X Window`、`KDE`、`GNOME`, etc.  

