

# 环境变量分类
可以分为 `全局变量` 和 `局部变量`  

# 变量基本操作
## 查看变量
使用 `printenv`/`env`/`set` 来进行查看  
`set` 会展示 全局、局部以及用户自定义变量  
`env`/`printenv` 则只会输出 全局变量  

## 设置变量
`VAR_NAME=VAR_VALUE` 方式可以实现变量的设置  

## 调用变量
`$VAR_NAME` 方式可以调用变量

## 扩展变量
`expore VAR_NAME` 会使当前shell的子shell都能访问该变量  

## 删除变量
`unset VAR_NAME` 可以删除环境变量  

# 环境变量规则
根据bash shell的启动方式不同，环境变量的确认方式也不尽相同  
这里分为 `登录shell`、`交互式shell`、`非交互式shell`  
## 登录shell
它会读取：
> /etc/profile
> 
> $HOME/.bash_profile
> 
> $HOME/.bashrc
> 
> $HOME/.bash_login
> 
> $HOME/.profile
>
`$HOME`打头的4个，大多的Linux发行版都只会用到其中的1～2个  
它们的被读取顺序是 `.bash_profile` \> `.bash_login` \> `.profile`  
`.bashrc` 不在其中，因为它通常通过其它文件运行，比如 CentOS 的 *.bash_profile* 里面会包含：  
```
if [ -f ~/.bashrc ]; then
    . ~/.bashrc
```

## 交互式shell
不是登录时启动的，但是有交互界面的bash shell，即为交互式shell（比如登录后在通过 `bash` 命令调出的shell）  
它不会访问 */etc/profile* 文件，只检查该用户家目录下的 *.bashrc* 文件  

## 非交互式shell
它没有命令提示符，系统执行shell脚本时使用的是这种shell  
它会检查 `BASH_ENV` 这个环境变量  

综上，  
对于全局环境变量，可以在 `/etc/profile.d` 目录下建立 *.sh* 结尾的文件，并在其中定义它们  
对于局部环境变量，将它们写到 `~/.bashrc` 中  

# 数组变量
变量支持定义数组  
```
# mytest=(one two three four)

# echo $mytest
one

# echo ${mytest[2]}
three

# echo $mytest[2]
one[2]
```
> <h2>⚠️注意：</h2>
> 
> 取数组中的值，必须要花括号包裹
> 
> 当然，也可以使用 `${mytest[@]}` 或者 `${mytest[*]}` 来取出所有的值
> 

